package com.zaahid.belajarlivedataactivity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.belajarlivedataactivity.R
import com.zaahid.belajarlivedataactivity.adapter.SecondAdapter
import com.zaahid.belajarlivedataactivity.databinding.FragmentKeduaBinding
import com.zaahid.belajarlivedataactivity.viewmodel.MainViewModel


class FragmentKedua : Fragment() {
    private var _binding : FragmentKeduaBinding? = null
    private val binding get()  = _binding!!
    private val model : MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKeduaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        model.curentData.observe(viewLifecycleOwner) {
//            val a = it.kata
//            val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
//            val adapter = SecondAdapter(a /* = java.util.ArrayList<kotlin.String> */)
//            binding.secondRV.layoutManager = layoutManager
//            binding.secondRV.adapter = adapter
//        }
        val a = model.array.value
        binding.textview.text = a.toString()
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val adapter = SecondAdapter(a as ArrayList<String> /* = java.util.ArrayList<kotlin.String> */)
        binding.secondRV.layoutManager = layoutManager
        binding.secondRV.adapter = adapter
//        model.array.observe(viewLifecycleOwner){
//            val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
//            val adapter = SecondAdapter(it /* = java.util.ArrayList<kotlin.String> */)
//            binding.secondRV.layoutManager = layoutManager
//            binding.secondRV.adapter = adapter
//        }


    }

}