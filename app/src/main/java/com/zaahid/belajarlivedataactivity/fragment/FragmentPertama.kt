package com.zaahid.belajarlivedataactivity.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.zaahid.belajarlivedataactivity.MainActivity
import com.zaahid.belajarlivedataactivity.R
import com.zaahid.belajarlivedataactivity.databinding.FragmentPertamaBinding
import com.zaahid.belajarlivedataactivity.viewmodel.MainViewModel


class FragmentPertama : Fragment() {
    private var _binding : FragmentPertamaBinding? = null
    private val binding get()  = _binding!!
    private val model : MainViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentPertamaBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        binding.buttoncari.setOnClickListener {
            val text = binding.texthuruf.text?.toString()
            if (text != null){
                val x = text.toString().lowercase().first()
                model.gantiData(x)
                it.findNavController().navigate(R.id.action_fragmentPertama_to_fragmentKedua)
            }else{
                Toast.makeText(context, "harap isi data terlebih dulu", Toast.LENGTH_SHORT).show()
            }
        }
    }


}