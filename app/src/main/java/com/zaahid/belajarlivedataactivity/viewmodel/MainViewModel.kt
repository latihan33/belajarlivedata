package com.zaahid.belajarlivedataactivity.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zaahid.belajarlivedataactivity.DataKata

class MainViewModel : ViewModel() {
    var mylist = arrayListOf(
        DataKata('a', arrayListOf("Abstract","Algorithm","Adapter","Array","Approach")),
        DataKata('b', arrayListOf("Both", "Base","Back-end","Bug","Boolean")),
        DataKata('c', arrayListOf("Code","Compile","Copy","CSS","Char")),
        DataKata('d', arrayListOf("Data","Debug","Class","Description","Default")),
        DataKata('e', arrayListOf("Exceptoion","Error","Example","Engine","Extend")),
        DataKata('f', arrayListOf("Fragment","Format","Final","Field","Front-end","Full-Stack")),
        DataKata('g', arrayListOf("Get","Grid","Gesture","Geometri","Glance")),
        DataKata('h', arrayListOf("Html","Hit","History","Host","Https","Hashcode")),
        DataKata('i', arrayListOf("Input","Instance","Integer","if","Integrity")),
        DataKata('j', arrayListOf("JQuery","Java","Json","JavaScript","Jar","Jaringan")),
        DataKata('k', arrayListOf("Key","Kotlin","kursus","Kind","Keep")),
        DataKata('l', arrayListOf("Library","List","Location","Legacy","Login","Lifecycle")),
        DataKata('m', arrayListOf("Methode","Message","Metadata","Menu","Media","Material")),
        DataKata('n', arrayListOf("Narsis","Nubuat","Nabi","Nirmala")),
        DataKata('o', arrayListOf("Organ","Output","Override","Object","Optional","Overlays"))
    )
    private var _curentData = MutableLiveData<DataKata>()
    val curentData : LiveData<DataKata> get() = _curentData

    private var _array = MutableLiveData<ArrayList<String>>()
    val array : LiveData<ArrayList<String>> get() = _array
    fun gantiData(x:Char){
        val b = mylist.indexOfFirst { it.huruf == x }
        if (b>=0){
            val data = mylist[b]
            _curentData.value = data
            _array.value = data.kata
        }
    }
}